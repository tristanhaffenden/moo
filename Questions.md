# Questions
#### _1. What did you think of the provided acceptance criteria?_

It was a bit too vague to use to determine the full extent of testing requirements and the actual criteria to test against.

For example there is no definition of what the 'invalid' search text should be (to make sure it's displaying the message everyone is expecting).
It doesn't define if the message should be localised (it's currently not, but other parts of search are). 

There was no aspect of search results pagination or ordering covered. 

'A view of products matching that search term' doesn't give any definitive guidance on what that view should be.

___
#### _2. If this were a real story from a real life Product Owner - what questions would you ask?_

1. Are we concerned with the actual results returned once the search is performed or just that it _does_ return results?
2. What is the expected 'view' format for multiple results?
3. If we do care about the results returned, how is search relevance/ordering dictated?
4. What's the maximum number of results we should see per page?
5. Do we provide any kind of suggestions for misspelled words we should be testing against?

___
#### _3. Why did you choose to structure your code in the way you did?_

I followed the page object model for ease of use and editing/re-usability. 
All pages inherit from a metaclass which provides easy access to some common repeatable actions and methods, 
without the user needing to import anything extra themselves.
Localisation files have been stored in config so that the page objects can be used in any language. 

Common, non-test specific functions are kept in the helpers directory and are intented to be generic enough that they can be used anywhere they are required.
Test specific functions are kept in the keywords directory. These are logical groupings of actions you would perform on the site.  

___
#### _4. If you had more time what improvements would you make to your code?_
* Configure it to work on any browser, not just chrome in docker.
* Automate the docker container creation and teardown, so the test runner will handle that for you.
* Take screenshots when tests fail.
* Abstract more logic from the tests into repeatable functions.
* Add full range of localised environments to the config files (with their translations).
* Add full doc strings (so I could add a tool like sphinx to automate the test documentation generation).

___
#### _5. What is your usual approach to testing on a project? (Hint: talk about different levels of testing you would do; who would collaborate with whom etc)_
That depends on the project really, but as an example here's somethings I would usually do when starting a new project in my current role:

* Review the initial project spec to ensure I understand both the functionality and the purpose of introducing this functionality.
* Highlight any gaps in the spec if there are any, and ask any questions about the functionality straight away, so they can be considered and answered as soon as possible. (This should involve everyone in the project/be documented somewhere anyone can read it). 
* Outline the phases of releases (will we release the functionality in one big release, or multiple smaller ones, so work can be prioritised accordingly). 
* Consider the impact this release may have on other releases planned for the same period, and if there could be any consequences from combining those (not so much of a problem in some architectures, but at my current position we're on once a week release cycles, quite often of things which have an element of overlap so it has been known to cause issues). 
* Discuss any changes to existing system functionality/API's which may be impacted by this project, to make sure we have appropriate tests in place to verify that old functionality is not broken.
* Work with the developers to organise the workflow of the project, to ensure the test automation is approached in a similar order as the functionality is built, to ensure it is providing benefit as soon as possible. e.g. If the API's will be built before the front end, implement any required API tests before moving on to the front end functional tests.
* Discuss any potential load or performance impact this functionality might have, and design any load test scenarios as appropriate.
* Ideally plan for time between initial 'completion' and release to conduct some exploratory testing, to get an idea for how it feels when you use it, and to try and break it in any ways that the automated tests might not be easily able to.

Ultimately I think the most successful projects I have worked on have been ones where everyone openly collaborated, so I think it's important that everyone in the project gets involved, so we can all be confident in the progress and drive of the project.
