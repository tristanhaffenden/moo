"""Helpers for any kind of file reading or writing."""
from ruamel import yaml


def read_yaml_file(file_path: str) -> dict:
    """Return contents of the file specified by ``file_path``."""
    with open(file_path, 'r') as contents:
        yaml_file = yaml.safe_load(contents)
    return yaml_file
