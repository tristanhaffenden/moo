from typing import Optional

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.expected_conditions import (
    visibility_of_element_located)
from selenium.webdriver.support.ui import WebDriverWait


def wait_until_element_is_visible(
        driver: WebDriver, locator: tuple, timeout: int,
        message: Optional[str] = None) -> None:
    """Wait until the element specified by locator is visible."""
    if message is None:
        message = f"'{locator}' not visible after {timeout}s"

    WebDriverWait(driver, timeout).until(
        visibility_of_element_located(locator), message)
