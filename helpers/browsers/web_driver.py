"""Web driver helpers.

For when you have repeatable interactions you might
want to use in multiple files..
"""
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.remote.webelement import WebElement


class WebDriverHelpers(object):
    """Common web driver based interactions.

    These are all available in any page objects which inherit from
    ``pages.page_object.PageObject`` using ``self.helpers``.
    """

    def __init__(self, root: WebElement) -> None:
        self.root = root

    def write_text(self, text: str, locator: tuple,
                   clear: bool = False) -> None:
        """Write ``text`` into the specified web element.

        Args:
            text: The text to enter into the element.
            locator: The locator tuple to find the element in which you want
                to enter the text.
            clear: If you want to clear the element before entering the text.

        """
        element = self.root.find_element(*locator)
        if clear is True:
            element.clear()
        element.send_keys(text)

    def is_displayed(self, locator: tuple) -> bool:
        """Check if an element is displayed.

        Includes handling for NoSuchElement errors, which obviously mean it's
        not displayed.

        Args:
            locator: Locator for the element to double click.

        """
        try:
            return self.root.find_element(*locator).is_displayed()
        except NoSuchElementException:
            return False
