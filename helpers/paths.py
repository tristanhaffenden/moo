"""Helpers for paths inside the project."""
from os import environ, path

ROOT = environ['VIRTUAL_ENV']

CONFIG_DIR = path.join(ROOT, 'config')

LOCALISATION_DIR = path.join(CONFIG_DIR, 'localisation')
DEFAULTS = path.join(CONFIG_DIR, 'defaults.yaml')
ENVS = path.join(CONFIG_DIR, 'envs.yaml')
