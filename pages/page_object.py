from abc import ABCMeta
from os import path
from typing import Dict, Optional, Union

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement

from helpers.browsers.wait import wait_until_element_is_visible
from helpers.browsers.web_driver import WebDriverHelpers
from helpers.file_io import read_yaml_file
from helpers.paths import LOCALISATION_DIR


class PageObject(metaclass=ABCMeta):
    """Super class for all page objects.

    This class provides the template for other page objects to use and build
    on, and easy access to helpers that your page objects may need to use.
    """

    locators = {}  # type: Dict[str, Union[tuple, str]]

    LOCALISATION_FILE = None

    def __init__(self, driver: WebDriver, env: dict) -> None:
        self.driver = driver
        self.root_url = env['url']
        self.timeout = env['timeout']
        self._url_args = None
        self.page_text = self._get_localised_text(lang=env['lang'])

    @property
    def url(self) -> Optional[str]:
        """Specify the expected URL for the page, if relevant.

        Should be overwritten by page objects when required.
        """
        return self.root_url

    @property
    def url_args(self) -> dict:
        """Url args dict.

        Required when the URL property requires additional arguments.

        Returns:
            A dictionary of the arguments required to form the URL.
            The dictionary must be set through this property's setter method.

        Raises:
            A ``ValueError`` if no args dict has been set before its used.
        """
        if self._url_args is None:
            raise ValueError('URL arguments are required to use this URL. '
                             'Please review the requirements in the doc '
                             'string for the page object.')
        return self._url_args

    @url_args.setter
    def url_args(self, kwargs: dict) -> None:
        """Set the URL args dict."""
        self._url_args = kwargs  # type: ignore

    @property
    def root(self) -> WebElement:
        """Specify a page object's root element.

        This is used so when searching a page you can easily do so from the
        page object's root element, meaning you don't have to search the
        entire page DOM.

        Example:
            Instead of searching for an element from the driver, you can search
            for an element from the root. e.g.:

            .. code-block:: python

                self.root.find_element(*self.locator['my_locator'])

        """
        try:
            return self.driver.find_element(*self.locators['root'])
        except KeyError:
            raise ValueError(f'No root element specified for '
                             f'{self.__class__.__name__}')

    def wait_for_load(self, message: Optional[str] = None) -> None:
        """Wait for the page object to be appropriately loaded.

        Should be overwritten inside your page object, if you want to wait for
        something other than root element visibility.
        """
        wait_until_element_is_visible(driver=self.driver, timeout=self.timeout,
                                      locator=self.locators['root'],
                                      message=message)

    @property
    def helpers(self) -> WebDriverHelpers:
        """Access helper functions for common web driver interactions.

        Example:
            .. code-block:: python

                self.helpers.write_text('foo', self.locators['bar'])

        Return:
            The web driver helpers class, with the ``driver`` and env
            ``values`` specified by the page object.

        """
        return WebDriverHelpers(root=self.root)

    def is_active(self) -> bool:
        """Specify if this page object is active."""
        try:
            return self.root.is_displayed()
        except NoSuchElementException:
            return False

    def _get_localised_text(self, lang: str) -> Optional[dict]:
        """Get the expected text for the page.

        Returns any text values from the localisation config file, based on
        the language specified for the environment.
        """
        if self.LOCALISATION_FILE is None:
            return None
        file_contents = read_yaml_file(
            file_path=path.join(LOCALISATION_DIR, self.LOCALISATION_FILE))
        try:
            return file_contents[lang]
        except KeyError:
            raise NotImplementedError(f'No translations stored for {lang} in '
                                      f'{self.LOCALISATION_FILE}')
