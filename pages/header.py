"""Page object for the site header."""
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver

from pages.page_object import PageObject


class PageHeader(PageObject):

    def __init__(self, driver: WebDriver, env: dict) -> None:
        super().__init__(driver, env)

    locators = {
        'root': (By.CSS_SELECTOR, '.header'),
        'search_input': (By.ID, 'query'),
        'search_button': (By.CSS_SELECTOR, '.search__btn'),
    }

    def enter_search_text(self, text: str) -> None:
        """Enter ``text`` to search for."""
        self.helpers.write_text(text=text, clear=True,
                                locator=self.locators['search_input'])

    def click_search_button(self) -> None:
        """Click the magnifying glass icon to search."""
        self.root.find_element(*self.locators['search_button']).click()
