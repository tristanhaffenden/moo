"""Page object for the original business cards page."""
from selenium.webdriver.remote.webdriver import WebDriver

from pages.page_object import PageObject


class BusinessCards(PageObject):

    def __init__(self, driver: WebDriver, env: dict) -> None:
        super().__init__(driver, env)

    @property
    def url(self):
        return f'{self.root_url}products/business-cards.html'
