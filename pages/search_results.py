"""Page object for the search results."""
from typing import List, Optional

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement

from helpers.browsers.wait import wait_until_element_is_visible
from pages.page_object import PageObject


class SearchResults(PageObject):

    def __init__(self, driver: WebDriver, env: dict) -> None:
        super().__init__(driver, env)

    locators = {
        'root': (By.CSS_SELECTOR, '.container'),
        'header': (By.CSS_SELECTOR, '.page-header'),
        'results_info': (By.ID, 'resInfo-0'),
        'search_results': (By.ID, 'search-results'),
        'no_results': (By.CSS_SELECTOR, '.gs-no-results-result'),
        'result_titles': (By.CSS_SELECTOR, 'div[class="gs-title"] a'),
        'contact_us': (By.CSS_SELECTOR, 'a[href^="https://support.moo.com"]'),
        'find_out_more': (By.CSS_SELECTOR, 'a[href="/next-day-delivery/"]'),
    }

    LOCALISATION_FILE = 'search.yaml'

    @property
    def url(self):
        query_string = self.url_args['query'].replace(' ', '+')
        return f'{self.root_url}search/?query={query_string}'

    def wait_for_load(self, message: Optional[str] = None):
        wait_until_element_is_visible(driver=self.driver, timeout=self.timeout,
                                      locator=self.locators['header'],
                                      message=message)

    def get_header_text(self) -> str:
        """Get the search results header text."""
        return self.root.find_element(*self.locators['header']).text

    def get_expected_header_text(self, search_term: str) -> str:
        """Return the expected (localised) header text."""
        return f'{self.page_text["results_header"]} "{search_term}"'

    def search_results_element_displayed(self):
        return self.helpers.is_displayed(
            locator=self.locators['search_results'])

    def get_expected_no_results_text(self) -> str:
        return self.page_text['no_results']

    def get_no_results_text(self) -> str:
        return self.root.find_element(*self.locators['no_results']).text

    def _search_results(self) -> WebElement:
        return self.root.find_element(*self.locators['search_results'])

    def get_all_search_result_elements(self) -> List[WebElement]:
        return self._search_results().find_elements(
            *self.locators['result_titles'])

    def contact_us_link_is_displayed(self) -> bool:
        return self.helpers.is_displayed(
            locator=self.locators['contact_us'])

    def next_day_delivery_link_is_displayed(self) -> bool:
        return self.helpers.is_displayed(
            locator=self.locators['find_out_more'])
