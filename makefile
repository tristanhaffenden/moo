
help:
	@echo "-------------------------------------------------------------------"
	@echo "Makefile Help"
	@echo "-------------------------------------------------------------------"
	@echo "Use this make file to automate any actions you make want to repeat"
	@echo "and easily run."
	@echo " "
	@echo "Add any of the following commands to perform actions:"
	@echo " "
	@echo "  clean"
	@echo "      Delete python artifacts in this project."
	@echo " "
	@echo "  container"
	@echo "      Download and start the required chrome docker container."
	@echo " "
	@echo "  virtualenv"
	@echo "      Create a virtual env to install the required packages into."
	@echo " "
	@echo "  install"
	@echo "      Install the project dependencies."
	@echo " "
	@echo "  test"
	@echo "      Run the tests."
	@echo " "

clean:
	find . -name '*.pyc' -exec rm -f {} +

container:
	docker run -d -p 4444:4444 -p 5900:5900 selenium/standalone-chrome-debug:3.6.0-copper

virtualenv:
	python3.6 -m venv .

install:
	pip install -r requirements.txt

test:
	pytest tests