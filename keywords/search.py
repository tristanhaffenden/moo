from selenium.webdriver.remote.webdriver import WebDriver

from pages.header import PageHeader


def search_for_text(driver: WebDriver, env: dict, text: str) -> None:
    """Search for text using the quick search in the site header."""
    header = PageHeader(driver, env)
    header.wait_for_load()
    header.enter_search_text(text=text)
    header.click_search_button()
