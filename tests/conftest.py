import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions

from helpers.file_io import read_yaml_file
from helpers.paths import DEFAULTS, ENVS


def pytest_addoption(parser):
    """Add options to use when running the tests."""
    defaults = read_yaml_file(DEFAULTS)

    parser.addoption(
        '--browser', dest='browser', action='store',
        default=defaults['browser'],
        help='Specify the browser you want to use'
    )

    parser.addoption(
        '--env', dest='env', action='store',
        default=defaults['env'],
        help='Specify the environment you want to run tests against'
    )


@pytest.fixture(scope='session', autouse=True)
def env():
    env_file = read_yaml_file(ENVS)
    yield env_file[pytest.config.option.env]


@pytest.fixture(scope='session', autouse=True)
def driver():
    browser = pytest.config.option.browser
    if browser == 'docker-chrome':
        options = ChromeOptions()
        _driver = webdriver.Remote(
            desired_capabilities=options.to_capabilities())
        _driver.maximize_window()
        yield _driver
        _driver.quit()
    else:
        raise NotImplementedError('Currently docker chrome is the only '
                                  'supported browser.')
