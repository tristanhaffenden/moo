from pytest import mark
from selenium.webdriver.remote.webdriver import WebDriver

from keywords.search import search_for_text
from pages.products.business_cards import BusinessCards
from pages.products.original_business_cards import OriginalBusinessCards
from pages.search_results import SearchResults


@mark.parametrize('search_term, return_results',
                  [('business cards', True),
                   ('sdjfnjsdfj', False)])
def test_search_functionality(driver: WebDriver, env: dict, search_term: str,
                              return_results: bool):
    """Testing the search functionality of the main moo site."""
    driver.get(env['url'])
    search_for_text(driver=driver, env=env, text=search_term)

    search_results = SearchResults(driver, env)
    search_results.wait_for_load()
    search_results.url_args = {'query': search_term}
    # correct url
    assert driver.current_url == search_results.url
    # header text is correct
    assert search_results.get_header_text() == \
        search_results.get_expected_header_text(search_term=search_term)
    # search result element is displayed
    assert search_results.search_results_element_displayed()

    all_results = search_results.get_all_search_result_elements()

    if return_results is True:
        # 10 results on first page
        assert len(all_results) == 10
        # verify links in results
        if search_term == 'business cards':
            assert all_results[0].get_attribute('href') == \
                OriginalBusinessCards(driver, env).url
            assert all_results[1].get_attribute('href') == \
                BusinessCards(driver, env).url
    else:
        assert len(all_results) == 0
        # No results text should be displayed.
        assert search_results.get_no_results_text() == \
            search_results.get_expected_no_results_text()

    # sections which should be displayed if there are results or not.
    assert search_results.contact_us_link_is_displayed()
    assert search_results.next_day_delivery_link_is_displayed()
