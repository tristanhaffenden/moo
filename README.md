# Moo Technical Test

## Requirements
* Docker
* Python 3.6
##### Optional
* VNC Viewer

	The docker container used is a debug one, so you can watch the test run if desired.
	To do this you will need to start a VNC viewer application after starting the container,
	and access it on port '5900'.

## Getting Started
To make the setup for this project simple the required commands have been added to a makefile.

To download and start the required docker container, run:
```
make container
```

Once the docker container is running, you need to create the python virtual environment:
```
make virtualenv
```

Then activate the virtual environment:
```
source bin/activate
```

Install the project dependencies:
```
make install
```

Then you're good to run the tests!

## Running Tests
A single command will run the two created tests:
```
make test
```
You will then see the output from pycharm in your shell.

Once the test run is completed, an html report will be created in the `results` directory.
